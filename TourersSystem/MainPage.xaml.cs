﻿using TourersSystem.Services;

namespace TourersSystem
{
    public partial class MainPage : ContentPage
    {
        private State state = State.Waiting;

        private string last = "";

        public MainPage()
        {
            InitializeComponent();
            Dispatcher.Dispatch(OnLoaded);
            ServerService.Instance.RunAsync();
            SensorService.Instance.RunAsync();
        }

        private async void OnLoaded()
        {
            var gradient1 = new GradientStop(Color.FromArgb("#a02a2a2a"), 0.0f);
            var gradient2 = new GradientStop(Color.FromArgb("#002a2a2a"), 0.1f);
            var gradient3 = new GradientStop(Color.FromArgb("#a02a2a2a"), 0.2f);
            rect.Background = new LinearGradientBrush()
            {
                StartPoint = new(0, 0),
                EndPoint = new(1, 1),
                GradientStops = { gradient1, gradient2, gradient3 }
            };
            rect.Animate("gradient1", x => gradient1.Offset = (float)x, 0.0, 1.0, 16, 2000, repeat: () => true);
            rect.Animate("gradient2", x => gradient2.Offset = (float)x, 0.1, 1.1, 16, 2000, repeat: () => true);
            rect.Animate("gradient3", x => gradient3.Offset = (float)x, 0.2, 1.2, 16, 2000, repeat: () => true);
            await Task.Delay(1000);
            Dispatcher.StartTimer(TimeSpan.FromSeconds(1), OnTick);
        }

        private bool OnTick()
        {
            Tick();
            return true;
        }

        private async void Tick()
        {
            if (ServerService.Instance.IsExists && state == State.Waiting)
            {
                state = State.Active;
                webview.Source = $"http://{last = ServerService.Instance.ServerAddress}";
                webview.Loaded += (x, y) => webview.Opacity = 1;
                await Task.Delay(200);
                await splash.FadeTo(0, 500);
            }
            if (!ServerService.Instance.IsExists && state == State.Active)
            {
                state = State.Waiting;
                await splash.FadeTo(1);
                webview.Source = null;
            }
            if (ServerService.Instance.IsExists && last != ServerService.Instance.ServerAddress)
            {
                state = State.Waiting;
                await splash.FadeTo(1);
                webview.Source = null;
            }
        }

        private enum State { Waiting, Active }
    }
}