﻿using Android.App;
using Android.Content.PM;
using Android.Net.Wifi;
using TourersSystem.Platforms.Android;
using TourersSystem.Services;

namespace TourersSystem
{
    [Activity(Theme = "@style/SplashTheme",
        ScreenOrientation = ScreenOrientation.Landscape,
        MainLauncher = true,
        TurnScreenOn = true,
        ConfigurationChanges = ConfigChanges.ScreenSize
        | ConfigChanges.Orientation
        | ConfigChanges.UiMode
        | ConfigChanges.ScreenLayout
        | ConfigChanges.SmallestScreenSize
        | ConfigChanges.Density)]
    public class MainActivity : MauiAppCompatActivity
    {
        protected override void OnStart()
        {
            base.OnStart();
            Window.AddFlags(Android.Views.WindowManagerFlags.Fullscreen);
            Window.AddFlags(Android.Views.WindowManagerFlags.KeepScreenOn);
        }
    }
}