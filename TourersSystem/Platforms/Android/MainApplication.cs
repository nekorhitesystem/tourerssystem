﻿using Android.App;
using Android.Runtime;
using Android.Webkit;
using TourersSystem.Platforms.Android;
using TourersSystem.Services;
using WebView = Android.Webkit.WebView;

namespace TourersSystem
{
    [Application]
    public class MainApplication : MauiApplication
    {
        public MainApplication(IntPtr handle, JniHandleOwnership ownership) : base(handle, ownership)
        {
            DependencyService.RegisterSingleton<IMulticastService>(new AndroidMulticastService(this));
            WebView.SetWebContentsDebuggingEnabled(true);
        }

        protected override MauiApp CreateMauiApp() => MauiProgram.CreateMauiApp();
    }
}