﻿using Android.Content;
using Android.Net.Wifi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TourersSystem.Services;

namespace TourersSystem.Platforms.Android
{
    public class AndroidMulticastService : IMulticastService
    {
        private readonly WifiManager manager;

        public AndroidMulticastService(Context context)
        {
            manager = context.GetSystemService(Context.WifiService) as WifiManager;
        }

        public IMulticastLock CreateMulticastLock()
        {
            return new MulticastLock(manager);
        }
    }

    public class MulticastLock : IMulticastLock
    {
        private readonly WifiManager.MulticastLock androidLock;
        private bool disposedValue;

        public MulticastLock(WifiManager manager)
        {
            androidLock = manager?.CreateMulticastLock("nekorhite lock");
            androidLock?.Acquire();
        }

        public async ValueTask<UdpReceiveResult> ReceiveAsync(int port, CancellationToken cancellationToken)
        {
            using var client = new UdpClient(new IPEndPoint(IPAddress.Any, port))
            {
                EnableBroadcast = true,
                MulticastLoopback = false,
            };
            return await client.ReceiveAsync(cancellationToken);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    androidLock?.Release();
                    androidLock?.Dispose();
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
