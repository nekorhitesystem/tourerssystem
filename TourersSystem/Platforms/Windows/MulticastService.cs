﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using TourersSystem.Services;

namespace TourersSystem.Platforms.Windows
{
    public class WindowsMulticastService : IMulticastService
    {
        public WindowsMulticastService()
        {
        }

        public IMulticastLock CreateMulticastLock()
        {
            return new MulticastLock();
        }
    }

    public class MulticastLock : IMulticastLock
    {
        private bool disposedValue;

        public MulticastLock()
        {
        }

        public async ValueTask<UdpReceiveResult> ReceiveAsync(int port, CancellationToken cancellationToken)
        {
            using var client = new UdpClient(new IPEndPoint(IPAddress.Any, port))
            {
                EnableBroadcast = true,
                MulticastLoopback = false,
            };
            return await client.ReceiveAsync(cancellationToken);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                }
                disposedValue = true;
            }
        }

        public void Dispose()
        {
            Dispose(disposing: true);
            GC.SuppressFinalize(this);
        }
    }
}
