﻿using TourersSystem.Services;

namespace TourersSystem
{
    public static class MauiProgram
    {

        public static MauiApp CreateMauiApp()
        {
            var builder = MauiApp.CreateBuilder();
            builder
                .UseMauiApp<App>()
                .ConfigureFonts(fonts =>
                {
                    fonts.AddFont("Agency-FB-Bold.ttf", "Agency-FB-Bold");
                    fonts.AddFont("IcoMoon.ttf", "icomoon");
                    fonts.AddFont("OpenSans-Regular.ttf", "OpenSansRegular");
                    fonts.AddFont("OpenSans-Semibold.ttf", "OpenSansSemibold");
                });
#if DEBUG
#endif

            return builder.Build();
        }
    }
}