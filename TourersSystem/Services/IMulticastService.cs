﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace TourersSystem.Services
{
    public interface IMulticastService
    {
        IMulticastLock CreateMulticastLock();
    }

    public interface IMulticastLock : IDisposable
    {
        ValueTask<UdpReceiveResult> ReceiveAsync(int port, CancellationToken cancellationToken);
    }
}
