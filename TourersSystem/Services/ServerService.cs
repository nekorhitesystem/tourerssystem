﻿using System.Diagnostics;
using System.Net;
using System.Net.Sockets;
using System.Security.Cryptography;

namespace TourersSystem.Services
{
    public class ServerService
    {
        public static ServerService Instance { get; } = new();

        private readonly HttpClient client = new() { Timeout = TimeSpan.FromSeconds(5) };

        private const string keyword = "#nekorider by kameme1208#";

        public string ServerAddress { get; private set; }

        public bool IsExists { get; private set; }

        private ServerService()
        {

        }

        public Task RunAsync()
        {
            return Task.Run(FindServerAsync);
        }

        private async Task FindServerAsync()
        {
            while (true)
            {
                try
                {
                    var service = DependencyService.Get<IMulticastService>();
                    using var multicast = service.CreateMulticastLock();
      
                    using var source = new CancellationTokenSource(TimeSpan.FromSeconds(5));
                    var result = await multicast.ReceiveAsync(65000, source.Token);
                    var address = result.RemoteEndPoint.Address.ToString();

                    // 受信アドレスが不一致で、既存サーバーのステータスが取得できない場合 -> IP変更の可能性
                    if (ServerAddress != address && !await CheckServerStatusAsync(ServerAddress))
                    {
                        IsExists = false;
                        // 受信バッファーの復号値がキーワードと一致し、ステータスが取得できた場合は車載用システムとみなしアドレス更新
                        if(result.Buffer.Decrypt() is keyword && await CheckServerStatusAsync(address))
                        {
                            ServerAddress = address;
                            IsExists = true;
                        }
                    }
                }
                catch (OperationCanceledException ex)
                {
                    Debug.WriteLine("[ServerService.cs] Task canceled. \n{0}", ex);
                }
                catch (Exception ex)
                {
                    Debug.WriteLine("[ServerService.cs] Exception handled. \n{0}", ex);
                    Debug.Assert(false); 
                }
                IsExists = await CheckServerStatusAsync(ServerAddress);
            }
        }

        private async ValueTask<bool> CheckServerStatusAsync(string address)
        {
            try
            {
                using var res = await client.GetAsync($"http://{address}/info/locale");
                return res.StatusCode == HttpStatusCode.OK;
            }
            catch
            {
                return false;
            }
        }
    }

    public static class Cryptor
    {
        public static ReadOnlyMemory<byte> Encrypt(this string datastring, string password = "nekopass")
        {
            using var aes = Aes.Create();
            using var memory = new MemoryStream();
            using var writer = new BinaryWriter(memory);
            using var deriver = new Rfc2898DeriveBytes(password, 32);
            writer.Write(deriver.Salt);
            writer.Write(aes.IV);
            aes.Key = deriver.GetBytes(32);
            using var stream = new CryptoStream(memory, aes.CreateEncryptor(), CryptoStreamMode.Write);
            using var binary = new BinaryWriter(stream);
            binary.Write(datastring);
            stream.FlushFinalBlock();
            return memory.GetBuffer().AsMemory(0..(int)memory.Position);
        }

        public static string Decrypt(this byte[] databytes, string password = "nekopass")
        {
            using var aes = Aes.Create();
            using var memory = new MemoryStream(databytes);
            using var reader = new BinaryReader(memory);
            using var deriver = new Rfc2898DeriveBytes(password, reader.ReadBytes(32));
            aes.IV = reader.ReadBytes(aes.IV.Length);
            aes.Key = deriver.GetBytes(32);
            using var stream = new CryptoStream(memory, aes.CreateDecryptor(), CryptoStreamMode.Read);
            using var binary = new BinaryReader(stream);
            return binary.ReadString();
        }
    }
}
