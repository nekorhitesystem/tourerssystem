﻿using Grpc.Core;
using Grpc.Net.Client;
using MagicOnion.Client;
using NekorhiteCore;
using NekorhiteCore.Messages;
using System.Diagnostics;

namespace TourersSystem.Services
{
    public class SensorService
    {
        public static SensorService Instance { get; } = new(ServerService.Instance);

        private readonly ServerService server;

        private bool isActive;

        private string last = "";

        private GrpcChannel channel;
        private ISensorModuleService service;
        private SensorSettingData settings = new(true, true, true, true);

        public double Rotate { get; private set; }

        private SensorService(ServerService server)
        {
            this.server = server;
        }

        public Task RunAsync()
        {
            return Start();
        }

        private async Task Start()
        {
            Accelerometer.ReadingChanged += AccelerometerChanged;
            Magnetometer.ReadingChanged += MagnetometerChanged;
            Gyroscope.ReadingChanged += GyroscopeChanged;
            Compass.ReadingChanged += CompassChanged;
            try { Accelerometer.Start(SensorSpeed.UI); } catch (FeatureNotSupportedException e) { Debug.WriteLine("Sensor is not Supported", e); }
            try { Magnetometer.Start(SensorSpeed.UI); } catch (FeatureNotSupportedException e) { Debug.WriteLine("Sensor is not Supported", e); }
            try { Gyroscope.Start(SensorSpeed.UI); } catch (FeatureNotSupportedException e) { Debug.WriteLine("Sensor is not Supported", e); }
            try { Compass.Start(SensorSpeed.UI); } catch (FeatureNotSupportedException e) { Debug.WriteLine("Sensor is not Supported", e); }
            try
            {
                _ = Task.Factory.StartNew(GeolocationLoop, TaskCreationOptions.LongRunning);
                while (true)
                {
                    try { await Check(); } catch { }
                    await Task.Delay(100);
                }
            }
            finally
            {
                Accelerometer.ReadingChanged -= AccelerometerChanged;
                Magnetometer.ReadingChanged -= MagnetometerChanged;
                Gyroscope.ReadingChanged -= GyroscopeChanged;
                Compass.ReadingChanged -= CompassChanged;

                if (Accelerometer.IsMonitoring) { Accelerometer.Stop(); }
                if (Magnetometer.IsMonitoring) { Magnetometer.Stop(); }
                if (Gyroscope.IsMonitoring) { Gyroscope.Stop(); }
                if (Compass.IsMonitoring) { Compass.Stop(); }
            }

        }

        private async Task Check()
        {
            if (server.IsExists && !isActive)
            {
                channel = GrpcChannel.ForAddress($"http://{last = server.ServerAddress}:65000", new() { Credentials = ChannelCredentials.Insecure });
                service = MagicOnionClient.Create<ISensorModuleService>(channel);
                settings = await service.GetSettingsAsync();
                isActive = true;
            }
            if (!server.IsExists && isActive || server.IsExists && last != server.ServerAddress)
            {
                isActive = false;
                channel.Dispose();
                service = null;
            }
        }

        private async Task GeolocationLoop()
        {
            while (true)
            {
                try
                {
                    var loc = await Geolocation.GetLocationAsync(new(GeolocationAccuracy.Best, TimeSpan.FromSeconds(10)));
                    if (settings.Compass || service is null || loc?.Course is null) return;
                    await service.SendCompassAsync(loc.Course ?? 0);
                }
                catch(Exception e) { }
                
            }

        }

        private void MagnetometerChanged(object sender, MagnetometerChangedEventArgs e)
        {
            if (settings.Magnetometer || service is null) return;
            var vec = e.Reading.MagneticField;
            service.SendMagnetometerAsync(new(vec.X, vec.Y, vec.Z));
        }

        private void CompassChanged(object sender, CompassChangedEventArgs e)
        {
            if (settings.Compass || service is null) return;
            Rotate = e.Reading.HeadingMagneticNorth;
            service.SendCompassAsync(Rotate);
        }

        private double radial = 0;
        private void AccelerometerChanged(object sender, AccelerometerChangedEventArgs e)
        {
            if (settings.Accelerometer || service is null) return;
            var vec = e.Reading.Acceleration;
            var rad = Math.Atan2(vec.X, vec.Z);
            radial += (rad - radial) / 1000;
            service.SendAccelerometerAsync(new(Math.Sin(rad - radial) * 2, vec.Y, 0 ));
        }

        private void GyroscopeChanged(object sender, GyroscopeChangedEventArgs e)
        {
            if (settings.Gyroscope || service is null) return;
            var vec = e.Reading.AngularVelocity;
            service.SendGyroscopeAsync(new(vec.X, vec.Y, vec.Z));
        }
    }
}
